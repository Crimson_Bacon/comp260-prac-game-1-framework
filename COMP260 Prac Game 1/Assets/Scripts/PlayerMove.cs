﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// use this for playermove later
// https://docs.unity3d.com/ScriptReference/KeyCode.html

public class PlayerMove : MonoBehaviour
{
    public Vector2 move;
    public Vector2 velocity; // in metres per second 
    public float maxSpeed = 5.0f;
    public int playerNumber = 0;
    public float acceleration = 1.0f;
    public float brake = 5.0f;
    public float turnSpeed = 30.0f;
    public float destroyRadius = 1.0f;
    private float speed = 0.0f;
    private BeeSpawner beeSpawner;

    // Use this for initialization
    void Start ()
    {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (playerNumber == 1)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                // destroy nearby bees
                beeSpawner.DestroyBees(transform.position, destroyRadius);
            }

            // the horizontal axis controls the turn
            float turn = Input.GetAxis("Horizontal");

            // turn the car
            transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime);

            // the vertical axis controls acceleration fwd/back
            float forwards = Input.GetAxis("Vertical");
            if (forwards > 0)
            {
                //accelerate forwards
                speed = speed + acceleration * Time.deltaTime;
            }
            else if (forwards < 0)
            {
                // accelerate backwards
                speed = speed - acceleration * Time.deltaTime;
            }
            else
            {
                // braking
                if (speed > 0)
                {
                    speed = speed - brake * Time.deltaTime;
                }
                else
                {
                    speed = speed + brake * Time.deltaTime;
                }
            }

            // clamp the speed
            speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

            // compute a vector in the up direction of length speed
            Vector2 velocity = Vector2.up * speed;

            // move the object
            transform.Translate(velocity * Time.deltaTime, Space.Self);
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Keypad0))
            {
                // destroy nearby bees
                beeSpawner.DestroyBees(transform.position, destroyRadius);
            }

            float turn = Input.GetAxis("Horizontal2");
            transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime);
            float forwards = Input.GetAxis("Vertical2");
            if (forwards > 0)
            {
                speed = speed + acceleration * Time.deltaTime;
            }
            else if (forwards < 0)
            {
                speed = speed - acceleration * Time.deltaTime;
            }
            else
            {
                if (speed > 0)
                {
                    speed = speed - brake * Time.deltaTime;
                }
                else
                {
                    speed = speed + brake * Time.deltaTime;
                }
            }
            speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
            Vector2 velocity = Vector2.up * speed;
            transform.Translate(velocity * Time.deltaTime, Space.Self);
        }
	}
}
