﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour
{
    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;

    // private state
    private float speed;
    private float turnSpeed;

    public Transform target;
    //public Transform target2;
    private Vector2 heading = Vector2.right;

    public ParticleSystem explosionPrefab;

	// Use this for initialization
	void Start ()
    {
        // find the player to set the target
        PlayerMove p = FindObjectOfType<PlayerMove>();
        target = p.transform;

        // bee initally moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
	}

    // Update is called once per frame
    void Update()
    {
        // get the vector from the bee to the target and normalise it
        Vector2 direction = target.position - transform.position;
        //Vector2 direction2 = target2.position - transform.position;

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        //Vector2 finalDirection;

        // if (direction.magnitude > direction2.magnitude)
        // {
        //     finalDirection = direction2;
        //     transform.Translate(heading * speed * Time.deltaTime);
        // }
        // else
        // {
        //     finalDirection = direction;
        //     transform.Translate(heading * speed * Time.deltaTime);
        // }
        // turn left or right
        if (direction.IsOnLeft(heading))
       {
           // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject);
    }
}
