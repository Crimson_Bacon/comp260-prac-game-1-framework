﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{
    public int nBees;
    public BeeMove beePrefab;
    public Rect spawnRect;
    public float xMin, yMin;
    public float width, height;
    public float beePeriod = 5;
    public float minBeePeriod = 1;
    public float maxBeePeriod = 10;
    private bool spawn = false;

	// Use this for initialization
	void Start ()
    {
        for (int i = 0; i < nBees; i++)
        {
            BeeMove bee = Instantiate(beePrefab);
            bee.transform.parent = transform;
            bee.gameObject.name = "Bee " + i;
            float x = spawnRect.xMin + Random.value * spawnRect.width;
            float y = spawnRect.yMin + Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);
        }
        StartCoroutine(beeTimer());
    }

    // Update is called once per frame
    void Update ()
    {
        beePeriod -= Time.deltaTime;
        if (beePeriod <= 0f)
        {
            BeeMove Bee = Instantiate(beePrefab);
            beePeriod = 5;
        }
        if (spawn == true)
        {
            BeeMove Bee = Instantiate(beePrefab);
            StartCoroutine(beeTimer());
        }
	}

    IEnumerator beeTimer()
    {
        spawn = false;
        yield return new WaitForSeconds(Random.Range(minBeePeriod, maxBeePeriod));
        spawn = true;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMin, spawnRect.yMin),
            new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMax, spawnRect.yMin),
            new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMax, spawnRect.yMax),
            new Vector2(spawnRect.xMin, spawnRect.yMin));
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMin, spawnRect.yMax),
            new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    public void DestroyBees (Vector2 centre, float radius)
    {
        // destroy all bees within 'radius' of 'centre'
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
